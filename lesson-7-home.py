######### the game ######
import random
from datetime import datetime
import time

def deco_count_time(f):
    def time_count(*arg):
        start = datetime.now()
        f(*arg)
        total_time = (datetime.now() - start)
        print("You play : " + str(total_time))
        with open("time_of_game", "w") as file:
            file.write("You play : " + str(total_time))
        return total_time
    return time_count


@deco_count_time
def the_game():
    # comp create a unique random number
    secret_num = random.sample(range(1, 9), 4)
    #show secret_num (cheet)
    print(secret_num)

    print("Try to guess a 4 mining number")
    try_num_user = input("Enter your 4 mining number: ")


    # iterate array or list
    def iteration_array(array: str) -> list:
        empty_array = []
        for i in array:
            empty_array.append(int(i))
        print("iteration_array empty_array ")
        print(empty_array)
        return(empty_array)


    def second_check(secret_number, entered_number):
        for index, elem in list(enumerate(secret_number)):
            for index2, num in list(enumerate(entered_number)):
                if secret_number == entered_number:
                    print("You win")
                    return exit
                elif (int(elem) == int(num) and (index == index2)):
                    print("You find a bull - " + str(num))
                elif int(elem) == int(num) and (index != index2):
                    print("You find a cow - " + str(num))

        print("Try agan to guess a number")
        next_number = input("Enter new number: ")
        first_check(secret_number, next_number)


    
    def first_check(secret_num, entered_number):
        entered_number = iteration_array(entered_number)
        if len(entered_number) < 4  or len(entered_number) > 4:
            print("You entered wrong count of numbers")
        else:
            if secret_num == entered_number:
                print("You win")
            else:
                second_check(secret_num, entered_number)


    first_check(secret_num, try_num_user)


the_game()

#################### task 2 ##########################
math_arg = [" ", "+", "-", "//", "/", "%", "*"]


# read file calc.txt
with open("calc.txt", "r") as file:
    array = []
    for i in file:
        array.append(list(i[:1-2].split(" ")))
    for x in array:
        # find errors and mistakes into file
        for argss in math_arg:
            if x[0] == argss:
                print("error in line 109")
                print(x[0])
                print(argss)
                continue
        
        if len(x) < 3:
            print("len(array): " + str(len(x)))
            print(x[0])
            continue

        # copmair arguments with math_arg array
        if x[1] == "+":
            print(int(x[0]) + int(x[2]))
        elif x[1] == "-":
            print(int(x[0]) - int(x[2]))
        elif x[1] == "//":
            print(int(x[0]) // int(x[2]))
        elif x[1] == "/":
            print(int(x[0]) / int(x[2]))
        elif x[1] == "*":
            print(int(x[0]) * int(x[2]))
        elif x[1] == "%":
            print(int(x[0]) % int(x[2]))
        else:
            pass